// Import dependencies
import React from 'react'
import { withRouter } from 'react-router-dom'

// Import components
import { Menu, Icon, Layout } from 'antd'

// Import styles
import './Sider.css'

const { Sider } = Layout


/*
    Sider component
*/
class SiderComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            collapsed: false
        }
    }

    onCollapse = (collapsed) => {
        this.setState({ collapsed })
    }

    render() {
        return (
            <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse} theme='light'>
                <Menu theme='light' defaultSelectedKeys={[this.props.selection]} mode='inline' className='slider__menu'>
                    <Menu.Item key='1' onClick={() => this.props.history.push('/')}>
                        <Icon type='user' className='sider__navigation-icon' />
                        <span>Users</span>
                    </Menu.Item>
                    <Menu.Item key='2' onClick={() => this.props.history.push('/visualisation')}>
                        <Icon type='pie-chart' className='sider__navigation-icon' />
                        <span>Charts</span>
                    </Menu.Item>
                </Menu>
            </Sider>
        )
    }
}

export default withRouter(SiderComponent)
