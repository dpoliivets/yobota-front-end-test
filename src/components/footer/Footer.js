// Import dependencies
import React from 'react'

// Import components
import { Layout } from 'antd'

const { Footer } = Layout


/*
    Footer component
*/
const FooterComponent = () => {
    return (
        <Footer>by Dmitry Poliyivets for Yobota</Footer>
    );
};

export default FooterComponent;
