// Import dependencies
import React from 'react'

// Import components
import { List, Descriptions, Empty } from 'antd'


/*
    Data list component
*/
export default class DataList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            pagesPerPage: 10
        }
        this.onPageSizeChange = this.onPageSizeChange.bind(this)
    }

    /*
        Changes how many items are displayed per page
    */
    onPageSizeChange = (current, size) => {
        this.setState({ pagesPerPage: size })
    }

    render() {
        return (
            <div>
                {
                    this.props.data ?
                        <List
                            itemLayout='horizontal'
                            dataSource={this.props.data}
                            pagination={{
                                pageSize: this.state.pagesPerPage,
                                defaultPageSize: 10,
                                hideOnSinglePage: true,
                                pageSizeOptions: ['10', '20', '30', '40'],
                                showSizeChanger: true,
                                onShowSizeChange: this.onPageSizeChange,
                                total: this.props.data.length,
                                showTotal: (total) => `Total ${total} items`
                            }}
                            renderItem={(item) => (
                                <List.Item key={item.id}>
                                    <Descriptions title={item.first_name + ' ' + item.last_name}>
                                        <Descriptions.Item label='DOB'>{item.date_of_birth}</Descriptions.Item>
                                        <Descriptions.Item label='Industry'>{item.industry}</Descriptions.Item>
                                        <Descriptions.Item label='Salary'>{'£ ' + item.salary}</Descriptions.Item>
                                        <Descriptions.Item label='Experience (Years)'>{item.years_of_experience}</Descriptions.Item>
                                        <Descriptions.Item label='Email'>{item.email}</Descriptions.Item>
                                    </Descriptions>
                                </List.Item>
                            )}
                        />
                        : 
                        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                    }
            </div>
        )
    }
}
