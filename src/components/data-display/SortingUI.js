// Import dependencies
import React from 'react'

// Import components
import { Button, Icon, Tooltip } from 'antd'

const ButtonGroup = Button.Group


/*
    UI for sorting functionality
*/
export default class SortingUI extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            date_of_birth: 'ASC',
            salary: 'ASC',
            industry: 'ASC'
        }
    }

    /*
        Function that sorts a field with 
        the name 'property'
    */
    sortField = (property) => {
        if (this.state[property] === 'ASC') {
            this.props.sortField(property, 'DESC')
            this.setState({ [property]: 'DESC' })
        } else {
            this.props.sortField(property, 'ASC')
            this.setState({ [property]: 'ASC' })
        }
    }

    render() {
        return (
            <div style={{ display: 'flex' }}>
                <ButtonGroup>
                    <Tooltip placement='top' title='Sort by Date of Birth'>
                        <Button onClick={() => this.sortField('date_of_birth')}>
                            DOB
                            <Icon type={this.state.date_of_birth === 'ASC' ? 'up' : 'down'} />
                        </Button>
                    </Tooltip>
                    <Tooltip placement='top' title='Sort by Income'>
                        <Button onClick={() => this.sortField('salary')}>
                            Income
                            <Icon type={this.state.salary === 'ASC' ? 'up' : 'down'} />
                        </Button>
                    </Tooltip>
                    <Tooltip placement='top' title='Sort by Industry'>
                        <Button onClick={() => this.sortField('industry')}>
                            Industry
                            <Icon type={this.state.industry === 'ASC' ? 'up' : 'down'} />
                        </Button>
                    </Tooltip>
                </ButtonGroup>
            </div>
        )
    }
}
