// Import dependencies
import React from 'react'

// Import components
import { Layout } from 'antd'

// Import styles
import './Header.css'

// Import images
import logo from '../../assets/svg/yobota-logo.svg'

const { Header } = Layout


/*
    Header component
*/
const HeaderComponent = () => {
    return (
        <Header className='header__container'>
            <img src={logo} className='slider__logo' alt='yobota-logo' />
        </Header>
    )
}

export default HeaderComponent
