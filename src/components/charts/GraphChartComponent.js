// Import dependencies
import React, { PureComponent } from 'react'

// Import components
import { Tooltip as TooltipTop, Select } from 'antd'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts'

// Import utilities
import { processForGraphChart } from '../../util/GraphChartProcessor'

const { Option } = Select
const lineColours = ['#2593FC', 'red', 'green']


/*
    A component that displays Graph chart
*/
export default class GraphChartComponent extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            years_of_experience: false,
            salary: false,
            date_of_birth: false,
            xAxis: 'salary',
            yAxis: ['years_of_experience']
        }
    }

    /*
        Handles changes to x-axis & x-axis selectors
    */
    handleChangeX = (value) => {
        this.setState({ xAxis: value })
    }
    handleChangeY = (value) => {
        this.setState({ yAxis: value })
    }

    /*
        Adds a particular property to current data (state)
    */
    addData(property) {
        // If not already processed
        if (!this.state[property]) {
            var extraData = processForGraphChart(property).slice(0, 100)

            if (this.state.data.length === 0) {
                this.setState({
                    data: extraData,
                    [property]: true
                })
            } else {
                // Merge based on id property
                var result = []
                this.state.data.forEach((itm, i) => {
                    result.push(Object.assign({}, itm, extraData[i]))
                })
                this.setState({
                    data: result,
                    [property]: true
                })
            }
        }
    }

    async componentDidMount() {
        // Initial graph
        await this.addData('salary')
        await this.addData('years_of_experience')
        this.addData('date_of_birth')
    }

    render() {
        return (
            <div>
                <div style={{ marginBottom: 55, display: 'flex' }}>
                    <div>
                        <h4>X-axis</h4>
                        <TooltipTop placement='top' title='Select X-axis'>
                            <Select defaultValue='salary' style={{ width: 120 }} onChange={(value) => this.handleChangeX(value)}>
                                <Option value='date_of_birth'>Age</Option>
                                <Option value='years_of_experience'>Experience</Option>
                                <Option value='salary'>Salary</Option>
                            </Select>
                        </TooltipTop>
                    </div>
                    <div style={{ marginLeft: 25 }}>
                        <h4>Y-axis</h4>
                        <TooltipTop placement='top' title='Can choose multiple Y-axis'>
                            <Select
                                mode='multiple'
                                style={{ minWidth: 120 }}
                                placeholder='Please select'
                                defaultValue={['years_of_experience']}
                                onChange={(value) => this.handleChangeY(value)}
                            >
                                <Option value='date_of_birth'>Age</Option>
                                <Option value='years_of_experience'>Experience</Option>
                                <Option value='salary'>Salary</Option>
                            </Select>
                        </TooltipTop>
                    </div>
                </div>

                <LineChart width={1000} height={300} data={this.state.data}>
                    <CartesianGrid strokeDasharray='3 3' />
                    <XAxis dataKey={this.state.xAxis} />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    {this.state.yAxis.map((item, i) => (
                        <Line type='monotone' dataKey={item} stroke={lineColours[i]} key={i} />
                    ))}
                </LineChart>

                <p>Showing only first 100 results</p>
            </div>
        )
    }
}
