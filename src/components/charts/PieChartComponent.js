// Import dependencies
import React, { PureComponent } from 'react'

// Import components
import { PieChart, Pie, Sector } from 'recharts'
import { Button, Tooltip } from 'antd'

// Import utilities
import { processForPieChart } from '../../util/PieChartProcessor'

const ButtonGroup = Button.Group

const renderActiveShape = (props) => {
    const RADIAN = Math.PI / 180
    const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props
    const sin = Math.sin(-RADIAN * midAngle)
    const cos = Math.cos(-RADIAN * midAngle)
    const sx = cx + (outerRadius + 10) * cos
    const sy = cy + (outerRadius + 10) * sin
    const mx = cx + (outerRadius + 30) * cos
    const my = cy + (outerRadius + 30) * sin
    const ex = mx + (cos >= 0 ? 1 : -1) * 22
    const ey = my
    const textAnchor = cos >= 0 ? 'start' : 'end'

    return (
        <g>
            <text x={cx} y={cy} dy={8} textAnchor='middle' fill={fill}>
                {payload.name.length > 20 ? payload.name.substr(0, 20)+'...' : payload.name}
            </text>
            <Sector cx={cx} cy={cy} innerRadius={innerRadius} outerRadius={outerRadius} startAngle={startAngle} endAngle={endAngle} fill={fill} />
            <Sector
                cx={cx}
                cy={cy}
                startAngle={startAngle}
                endAngle={endAngle}
                innerRadius={outerRadius + 6}
                outerRadius={outerRadius + 10}
                fill={fill}
            />
            <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill='none' />
            <circle cx={ex} cy={ey} r={2} fill={fill} stroke='none' />
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill='#333'>{`Occurences ${value}`}</text>
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill='#999'>
                {`(Rate ${(percent * 100).toFixed(2)}%)`}
            </text>
        </g>
    )
}


/*
    A component that displays Pie chart
*/
export default class PieChartComponent extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            activeIndex: 0,
            industry: null,
            date_of_birth: null,
            years_of_experience: null
        }
    }

    /*
        Executes on hover of the pie chart
    */
    onPieEnter = (data, index) => {
        this.setState({
            activeIndex: index
        })
    }

    /*
        Changes data in pie chart
    */
    onChangeProperty = (property) => {
        // Check if data is already processed
        if (this.state[property] === null) {
            let result = processForPieChart(property)
            this.setState({
                [property]: result,
                currentData: result
            })
        } else {
            this.setState({ currentData: this.state[property] })
        }
    }

    componentDidMount() {
        this.onChangeProperty('industry')
    }

    render() {
        return (
            <div>
                <ButtonGroup style={{ marginTop: 10 }}>
                    <Tooltip placement='top' title='Visualise Industry'>
                        <Button onClick={() => this.onChangeProperty('industry')}>Industry</Button>
                    </Tooltip>
                    <Tooltip placement='top' title='Visualise Age'>
                        <Button onClick={() => this.onChangeProperty('date_of_birth')}>Age</Button>
                    </Tooltip>
                    <Tooltip placement='top' title='Visualise Experience'>
                        <Button onClick={() => this.onChangeProperty('years_of_experience')}>Experience</Button>
                    </Tooltip>
                </ButtonGroup>

                <PieChart width={600} height={470}>
                    <Pie
                        activeIndex={this.state.activeIndex}
                        activeShape={renderActiveShape}
                        data={this.state.currentData}
                        innerRadius={110}
                        outerRadius={150}
                        fill='#2593FC'
                        dataKey='value'
                        onMouseEnter={this.onPieEnter}
                    />
                </PieChart>
            </div>
        )
    }
}
