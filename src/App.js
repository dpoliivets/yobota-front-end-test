// Import dependencies
import React from 'react'
import { Switch, Route } from 'react-router-dom'

// Import routes
import DataDisplay from './routes/DataDisplay'
import DataVisualisation from './routes/DataVisualisation'

// Import styles
import './App.css'


function App() {
    return (
        <Switch>
            <Route exact path='/' component={DataDisplay} />
            <Route exact path='/visualisation' component={DataVisualisation} />
        </Switch>
    )
}

export default App
