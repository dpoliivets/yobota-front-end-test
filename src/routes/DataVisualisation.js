// Import dependencies
import React from 'react'

// Import components
import { Layout, PageHeader, Tabs } from 'antd'
import Sider from '../components/sider/Sider'
import Header from '../components/header/Header'
import Footer from '../components/footer/Footer'
import PieChartComponent from '../components/charts/PieChartComponent'
import GraphChartComponent from '../components/charts/GraphChartComponent'

// Import styles
import './DataDisplay.css'

const { Content } = Layout
const { TabPane } = Tabs


/*
    Data Visualisation display route
*/
export default class DataVisualisation extends React.Component {
    render() {
        return (
            <Layout className='data-display__container'>
                {/* Left Sider Component */}
                <Sider selection='2' />

                {/* Main Layout */}
                <Layout className='data-display__main-layout'>
                    <Header />

                    {/* User data as a list */}
                    <Content className='data-display__content-container'>
                        <PageHeader title='Visualisation tools' />

                        <div className='data-display__content-card'>
                            <Tabs defaultActiveKey='1'>
                                <TabPane tab='Graph Chart' key='1'>
                                    <GraphChartComponent />
                                </TabPane>
                                <TabPane tab='Pie Chart' key='2'>
                                    <PieChartComponent />
                                </TabPane>
                            </Tabs>
                        </div>
                    </Content>

                    <Footer />
                </Layout>
            </Layout>
        )
    }
}
