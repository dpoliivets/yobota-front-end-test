// Import dependencies
import React from 'react'

// Import components
import { Layout, Skeleton, Input, PageHeader, BackTop } from 'antd'
import Sider from '../components/sider/Sider'
import Header from '../components/header/Header'
import Footer from '../components/footer/Footer'
import DataList from '../components/data-display/DataList'
import SortingUI from '../components/data-display/SortingUI'

// Import styles
import './DataDisplay.css'

// Import utilities
import { createSorter } from '../util/Sort'
import { createFilter } from '../util/Filter'

const { Content } = Layout
const { Search } = Input

// Import mock data
const data = require('../data/MOCK_DATA.json')


/*
    Data display route
*/
export default class DataDisplay extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isDataLoading: true
        }
        this.sortField = this.sortField.bind(this)
        this.filterName = this.filterName.bind(this)
    }

    /*
        Sorts data based on the passed config
    */
    sortData(data, config) {
        if (data && data.length) {
            if (Array.isArray(config) && config.length) {
                data.sort(createSorter(...config))
            }
        }
        return data
    }
    /*
        Passes config to the sorting function
    */
    sortField = (property, direction) => {
        let result = this.sortData(data, [{ property: property, direction: direction }])
        this.setState({ data: result })
    }

    /*
        Filters first & last name according to the
        search string (keyWord)
    */
    filterData(data, keyWord) {
        if (data && data.length > 0) {
            if (keyWord !== '') {
                return data.filter(createFilter(keyWord))
            }
        }
    }
    /*
        Triggers filter function
    */
    filterName = (keyWord) => {
        if (keyWord === '') {
            this.setState({ data: data })
            return
        }

        var result = this.filterData(data, keyWord)
        this.setState({ data: result })
    }

    /*
        Loads data from local JSON
    */
    componentDidMount() {
        // Set state with JSON
        this.setState({ data: data, isDataLoading: false })
    }

    render() {
        return (
            <Layout className='data-display__container'>
                {/* Left Sider Component */}
                <Sider selection='1' />

                {/* Main Layout */}
                <Layout className='data-display__main-layout'>
                    <Header />

                    {/* User data as a list */}
                    <Content className='data-display__content-container'>
                        <PageHeader title='List of users'/>

                        <div className='data-display__content-card'>
                            <div className='data-display__functionality-container'>
                                {/* Sorting buttons */}
                                <SortingUI sortField={this.sortField} />

                                {/* Search input field */}
                                <Search
                                    placeholder='search by name'
                                    onChange={(e) => this.filterName(e.target.value)}
                                    enterButton
                                    className='data-display__search-input'
                                    allowClear
                                />
                            </div>

                            {/* A list which displays user data */}
                            {this.state.isDataLoading ? <Skeleton active /> : <DataList data={this.state.data} />}
                        </div>
                    </Content>

                    <Footer />
                    
                    <BackTop />
                </Layout>
            </Layout>
        )
    }
}
