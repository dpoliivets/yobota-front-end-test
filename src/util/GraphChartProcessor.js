const data = require('../data/MOCK_DATA.json')

const processForGraphChart = (property) => {
    var propertyValues = []

    // Prepare data
    for (let key in data) {
        // Skip loop if the property is from prototype
        if (!data.hasOwnProperty(key)) continue
        // Filter null objects
        if (data[key][property] === null) continue

        let currentValue = data[key][property]
        if (property === 'date_of_birth') {
            // Convert to date
            let correctDate = new Date(currentValue.slice(6, 10), parseInt(currentValue.slice(3, 5)) - 1, currentValue.slice(0, 2))
            let ageDifMs = Date.now() - correctDate.getTime()
            currentValue = Math.abs(new Date(ageDifMs).getUTCFullYear() - 1970)
        }

        propertyValues.push({ id: data[key].id, [property]: currentValue })
    }

    return propertyValues
}

export { processForGraphChart }
