const doFilter = (item, keyWord) => {
    if (!(keyWord instanceof RegExp)) {
        keyWord = new RegExp(keyWord, 'i')
    }

    var searchString = item.first_name+' '+item.last_name

    return keyWord.test(searchString);
}

const createFilter = (keyWord) => {
    return (item) => doFilter(item, keyWord)
}

export { createFilter }
