const data = require('../data/MOCK_DATA.json')

const processForPieChart = (property) => {
    var extractedData = {}
    var result = []

    // Count instances of the property
    for (let key in data) {
        // Skip loop if the property is from prototype
        if (!data.hasOwnProperty(key)) continue
        if (data[key][property] === null) continue

        let currentValue = data[key][property]

        if (property === 'date_of_birth') {
            // Convert to date
            let correctDate = new Date(currentValue.slice(6, 10), parseInt(currentValue.slice(3, 5)) - 1, currentValue.slice(0, 2))
            let ageDifMs = Date.now() - correctDate.getTime()
            currentValue = Math.abs(new Date(ageDifMs).getUTCFullYear() - 1970)
        }

        if (extractedData[currentValue]) extractedData[currentValue] += 1
        else extractedData[currentValue] = 1
    }

    // Prepare data
    for (let key in extractedData) {
        result.push({ name: key, value: extractedData[key] })
    }

    return result
}

export { processForPieChart }
