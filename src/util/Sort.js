// Define regex for matching dates
const dateRegex = /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/

const dirMap = {
    // greater-than
    gt: { asc: 1, desc: -1 },
    // less-than
    lt: { asc: -1, desc: 1 }
}

const doSort = (A, B, property, direction = 'ASC') => {
    var a = A[property]
    var b = B[property]
    
    // Check current field is empty
    if (a === null || b === null) {
        return;
    }

    // Determine if current property is a date
    if (a.toString().match(dateRegex)) {
        a = new Date(a.slice(6, 10), parseInt(a.slice(3, 5))-1, a.slice(0, 2))
        b = new Date(b.slice(6, 10), parseInt(b.slice(3, 5))-1, b.slice(0, 2))
    }

    if (a < b) {
        return dirMap.lt[direction.toLowerCase()]
    }
    if (a > b) {
        return dirMap.gt[direction.toLowerCase()]
    }
    return 0
}

const createSorter = (...args) => {
    if (typeof args[0] === 'string') {
        args = [
            {
                direction: args[1],
                property: args[0]
            }
        ]
    }

    return (A, B) => {
        let ret = 0

        args.some((sorter) => {
            const { property, direction = 'ASC' } = sorter
            const value = doSort(A, B, property, direction)

            if (value === 0) {
                // they are equal, continue to next sorter if any
                return false
            } else {
                // they are different, stop at current sorter
                ret = value
                return true
            }
        })

        return ret
    }
}

export { createSorter }
