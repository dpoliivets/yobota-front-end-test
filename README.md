# Yobota Front-End Test
by Dmytro Poliyivets.

### Preview
If you don't want to download the repo and install the dependencies, you can preview the result on Netlify [yobota-front-end-test.netlify.com](https://yobota-front-end-test.netlify.com/)

### Installation

You’ll need to have Node 8.16.0 or Node 10.16.0 or later version on your local machine to run this code.

Install the dependencies and start the server.

```sh
$ cd yobota-front-end-test
$ npm install
$ npm start
```

Any issues, please let me know.